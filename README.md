## XML 1C Parser

For cloning project
1. git clone [project]

For downloading dependencies
2. composer install

For creating tables
3. php artisan migrate

For running command to handle files
4. php artisan handle:products

For starting serve and go to web browser localhost:8080
5. php artisan serve --port=8080


