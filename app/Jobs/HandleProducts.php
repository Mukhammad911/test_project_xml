<?php

namespace App\Jobs;

use Orchestra\Parser\Xml\Facade as XmlParser;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use App\Models\Products;
use App\Models\Regions;
ini_set('memory_limit', '-1');


class HandleProducts
{
    private $handleImports = null;
    private $handleOffers = null;
    private $region_id = null;

    public function __construct()
    {
        for($i = 0; $i < 8; $i++)
        {
            $import_file = Storage::exists('import'.$i.'_1.xml');
            $offers_file = Storage::exists('offers'.$i.'_1.xml');
           
            if(!$import_file || !$offers_file)
            {
                Log::warning("The index file imports or offers is not exists. Index: ". $i);
                continue;
            }

            Log::info("The index will handling: ". $i);
            
            $this->handleImports = Storage::disk('local')->get('import'.$i.'_1.xml');

            $this->handleOffers = Storage::disk('local')->get('offers'.$i.'_1.xml');

            $this->startHandleImports();

            $this->startHandleOffers();

        }

    }

    protected function startHandleImports()
    {
        Log::info("[startHandleImports]");

        //Load String xml data
        Log::info("[startHandleImports][Load String xml Data Imports]");
        $xml_imports = simplexml_load_string($this->handleImports);
       

        //Json Encode xml data
        Log::info("[startHandleImports][Json Encode Xml Data imports]");
        $json_imports = json_encode($xml_imports);


        //Convert to array and also to collect
        Log::info("[startHandleImports][Convert to array and also to collect imports]");
        $array_imports = collect(json_decode($json_imports, TRUE));

       

        // We will get the name of files
        $catalog = $array_imports->get("Каталог");

        if(empty($catalog) || is_null($catalog))
        {
            Log::info("The catalog is empty");
            return;
        }

        //Getting Region Id
        $this->region_id = $this->checkingRegionId($catalog);

        //We will get of products count
        $products = $catalog['Товары'];

        if(empty($products) || is_null($products))
        {
            Log::info("The catalog has none products");
            return;
        }

        $this->matchImportProducts($products);

    }

    protected function matchImportProducts($products)
    {
        Log::info("[matchImportProducts]");
        if(count($products) == 0)
        {
            Log::info("[matchImportProducts][The products count is] : 0");
            return false;
        }

        foreach($products["Товар"] as $product)
        {
            Log::info("[Begining created record]");

            try{
                
                $code = $product["Код"];
                $name = $product["Наименование"];
                $weight = $product["Вес"];

                //check if product exist
                $exist_product = Products::where('code', $code)->where('region_id', $this->region_id)->first();
                
                if(is_null($exist_product))
                {
                    $new_product = new Products();
                    $new_product->code = $code;
                    $new_product->name = $name;
                    $new_product->weight = $weight;
                    $new_product->quantity = 0;
                    $new_product->price = 0;
                    $new_product->region_id = $this->region_id;

                    Log::info("[matchImportProducts][Create Product][Code]: ". $code);
                    Log::info("[matchImportProducts][Create Product][Name]: ". $name);
                    $new_product->save();
                    continue;
                }

                $exist_product->name = $name;
                $exist_product->weight = $weight;
                $exist_product->region_id = $this->region_id;

                $exist_product->save();

            }catch(\Exception $e){
                Log::error("The error handled while getting parameters");
                Log::error($e->getMessage());
                continue;
            }
        }

    }

    protected function startHandleOffers()
    {
        Log::info("[startHandleOffers]");
         
        Log::info("[startHandleOffers][Load String xml Data Offers]");
        $xml_offers = simplexml_load_string($this->handleOffers);

        Log::info("[startHandleOffers][Json Encode Xml Data offers]");
        $json_effors = json_encode($xml_offers);

        Log::info("[startHandleOffers][Convert to array and also to collect offers]");
        $array_effors = collect(json_decode($json_effors, TRUE));

        // We will get the name of files
        $packet = $array_effors->get("ПакетПредложений");

        if(empty($packet) || is_null($packet))
        {
            Log::info("[startHandleOffers] [The packet is empty]");
            return;
        }

         //We will get of products count
        $products = $packet['Предложения'];

        if(empty($products) || is_null($products))
        {
            Log::info("[startHandleOffers][The packet has none products]");
            return;
        }

        $this->matchOffersProducts($products);

    }

    protected function matchOffersProducts($products)
    {
        Log::info("[matchOffersProducts]");

        if(count($products) == 0)
        {
            Log::info("[matchOffersProducts] [The products count is 0]");
            return false;
        }

        foreach($products["Предложение"] as $product)
        {
            Log::info("[matchOffersProducts][Begining update offers]");

            try{
                
                $code = $product["Код"];
                $quantity = $product["Количество"];
                
                //Getting price
                $price = $product["Цены"]["Цена"][0]["ЦенаЗаЕдиницу"];

                //check if product exist
                $exist_product = Products::where('code', $code)->where('region_id', $this->region_id)->first();
                
                if(is_null($exist_product))
                {
                    Log::error("[matchOffersProducts][exist product is null] Product Code: ". $code);
                    continue;
                }
                Log::info("[matchOffersProducts][Updating price and Quantity] Product Code: ". $code);
                Log::info("[matchOffersProducts][Updating price and Quantity] Product Quantity: ". $quantity);
                Log::info("[matchOffersProducts][Updating price and Quantity] Product price: ". $price);

                $exist_product->quantity = $quantity;
                $exist_product->price = $price;

                $exist_product->save();

            }catch(\Exception $e){
                Log::error("The error handled while getting parameters");
                Log::error($e->getMessage());
                continue;
            }
        }

    }

    protected function checkingRegionId($catalog)
    {
        try{
            $region_name = $catalog["Наименование"];
            
            //Check if regions is exist get region Id
            $region = Regions::where('name', $region_name)->first();

            if(is_null($region))
            {
                $new_region = new Regions();
                $new_region->name = $region_name;
                
                $new_region->save();

                return $new_region->id;
            }

            return $region->id;
        }catch(\Exception $e){
            Log::info("The error was occured when getting region id");
            Log::info($e->getMessage());
            die;
        }
    }
}