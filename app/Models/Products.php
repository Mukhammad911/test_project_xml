<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    public function regions()
    {
        return $this->hasOne('\App\Models\Regions', 'id', 'region_id');
    }
}
