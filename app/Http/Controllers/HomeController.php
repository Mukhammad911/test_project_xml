<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Products;

class HomeController extends Controller
{
    public function index()
    {
        $datas = Products::paginate(20);
        return view('index', ['datas' => $datas]);
    }
}
